FROM debian:buster-backports AS base
RUN apt-get update && \
    apt-get install -qy auto-apt-proxy && \
    apt-get install -qy -t buster-backports \
        gunicorn \
        python3-django \
        && \
    apt-get install -qy \
        fonts-fork-awesome \
        gettext \
        libjs-bootstrap4 \
        libjs-moment \
        python3-django-countries \
        python3-pkg-resources \
        python3-six \
        python3-yaml \
        python3-whitenoise \
        wget \
    && wget https://people.debian.org/~terceiro/wafer-debconf-debs/libjs-moment-timezone_0.5.31+dfsg1-2_all.deb && apt-get install ./libjs-moment-timezone_0.5.31+dfsg1-2_all.deb && rm -f ./libjs-moment-timezone_0.5.31+dfsg1-2_all.deb \
    && echo "Base packages installed"

FROM base AS build
RUN apt-get install -qy \
        git \
        python3-pip \
        rsync \
    && \
    echo "Build packages installed"

# stuff from pypi
COPY requirements-base.txt ./requirements-base.txt
RUN pip3 install -r requirements-base.txt
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt && \
    rm -rf /root/.cache

# FIXME remove this when wafer > 0.7.7 is out and we are using pypi releases
RUN cd /usr/local/lib/python3.*/dist-packages && \
    django-admin compilemessages

FROM base
WORKDIR /app
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/bin /usr/local/bin
COPY . ./

ENV DATADIR=/data
RUN ln -sfT /data/localsettings.py localsettings.py
RUN ./manage.py collectstatic --no-input -v 0 && ./manage.py compress --force

USER nobody
CMD ./manage.py migrate && \
    ./manage.py createcachetable && \
    ./manage.py create_debconf_groups && \
    ./manage.py init_minidc_menu_pages && \
    gunicorn --workers=2 --threads=4 --worker-class=gthread wsgi
